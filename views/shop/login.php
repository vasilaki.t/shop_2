<?php

/***
 * @var $model app\models\LoginForm
 */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;


echo Html::tag('h1', 'Login');

echo Html::endTag('h1');
$form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 col-form-label'],
    ],
]);

echo $form->field($model, 'username')->textInput(['autofocus' => true]);
echo $form->field($model, 'password')->passwordInput();
echo Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']);

ActiveForm::end();

