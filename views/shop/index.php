<?php
/**
 * @var $orderForm app\models\OrderForm
 */

use app\api\CartApi;
use yii\bootstrap4\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;


echo GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'label' => 'Код товара',
            ],
            [
                'attribute' => 'car',
                'label' => 'Для авто',
            ],
            [
                'attribute' => 'product',
                'label' => 'Тип детали',
            ],

            [
                'label' => 'Кол-во',
                'attribute' => 'quantity',
                'value' => function ($data) {
                    if (!empty($data['remain'])) {
                        return $data['remain']['quantity'] - $data['remain']['reserve'];
                    }

                },
            ],
            [
                'label' => 'Цена',
                'attribute' => 'price',
                'value' => function ($row) {
                    return number_format($row['price'], 2);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{add2order}',
                'buttons' => [
                    'add2order' => function ($url, $model, $key) {
                        return Html::input('number', sprintf('cart[%d]', $model['id']), CartApi::getProduct($model['id']), [
                            'onchange' => sprintf('$.get("%s", {quantity: $(this).val(),id:%d});', \yii\helpers\Url::to(['order/change-cart']), $model['id']),
                            'max' => $model['remain']['quantity'] - $model['remain']['reserve'],
                        ]);
                    }
                ]
            ],
        ],
    ]
);

echo LinkPager::widget([
    'pagination' => $pages,
]);
echo Html::tag('br');
echo Html::tag('br');


Modal::begin([
    'title' => 'Создание заказа',
    'toggleButton' => ['label' => 'Оформить'],
]);


$form = ActiveForm::begin([
    'id' => 'order-form',
    'options' => [
        'class' => 'form-horizontal',
        'onsubmit' => '$.post( "' . \yii\helpers\Url::to(['order/order']) . '", $(this).serialize(), function( data ) {console.log(data)});return;'
    ],
]) ?>


<?= $form->field($orderForm, 'name') ?>
<?= $form->field($orderForm, 'email') ?>

<?= Html::submitButton('Заказать', ['class' => 'btn btn-primary']) ?>
<?php
ActiveForm::end();
Modal::end();

?>


