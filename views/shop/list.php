<?php


use app\api\ShopApi;
use yii\grid\GridView;
use yii\helpers\Html;


echo GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'label' => 'Код заказа',
            ],
            [
                'attribute' => 'user',
                'label' => 'Пользователь',
            ],
            [
                'attribute' => 'create_at',
                'label' => 'Дата создания',
            ], [
                'attribute' => 'status',
                'label' => 'Статус заказа',
                'value' => function ($model) {
                    return ShopApi::getOrderStatusName($model['status']);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{add2order}',
                'buttons' => [
                    'add2order' => function ($url, $model, $key) {
                        if($model['status'] == ShopApi::ORDER_STATUS_NEW || 1){
                            return Html::button('Оплатить', [
                                'onclick' => sprintf('$.get("%s");', \yii\helpers\Url::to(['order/pay','id' => $model['id']])),
                            ]);
                        }
                    }
                ]
            ],
        ],
    ]
); ?>


