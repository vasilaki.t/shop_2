<?php

namespace app\api;

use GuzzleHttp\Exception\GuzzleException;
use yii\helpers\Json;

/**
 * Class ShopApi
 */
class ShopApi
{

    const ORDER_STATUS_NEW = 'new';
    const ORDER_STATUS_CANCEL = 'cancel';
    const ORDER_STATUS_PAYED = 'payed';
    const ORDER_STATUS_IMPOSSIBLE = 'impossible';

    /**
     * @var \Psr\Http\Client\ClientInterface
     */
    private $client;

    /**
     * @var \app\dto\ApiDto
     */
    private $settings;


    /**
     * ShopApi constructor.
     * @param \Psr\Http\Client\ClientInterface $client
     */
    public function __construct(\Psr\Http\Client\ClientInterface $client, \app\dto\ApiDto $dto)
    {
        $this->client = $client;
        $this->settings = $dto;
    }

    /**
     * get order status list
     * @return array
     */
    public static function getOrderStatusList()
    {
        return [
            self::ORDER_STATUS_NEW => 'Новый',
            self::ORDER_STATUS_CANCEL => 'Отменен',
            self::ORDER_STATUS_PAYED => 'Оплачен',
            self::ORDER_STATUS_IMPOSSIBLE => 'Не хватка товаров',
        ];
    }

    /**
     * get order status name
     * @param $code
     * @return mixed|null
     */
    public static function getOrderStatusName($code)
    {
        $orderStatusList = self::getOrderStatusList();
        return key_exists($code, $orderStatusList) ? $orderStatusList[$code] : null;
    }

    /**
     * @param $url
     * @param null $page
     * @return mixed
     */
    private function request($url, $page = null)
    {
        try {
            $response = $this->client->request(
                'GET',
                $url . ($page ? '?page=' . $page : ''),
                [
                    'headers' => [
                        'Authorization' => sprintf('Bearer %s', $this->settings->getToken())
                    ]
                ]
            );
            $response->getStatusCode();
            $result = Json::decode($response->getBody()->getContents());
        } catch (GuzzleException $exception) {
            $result['error'] = $exception->getMessage();
        }
        return $result;
    }

    /**
     * @param integer null $page
     * @return array|mixed
     */
    public function getProductList($page = null)
    {
        $result = $this->request($this->settings->getUrl() . 'products', $page);
        return $result;
    }

    /**
     * @param null $page
     * @return mixed
     */
    public function getOrderList($page = null)
    {
        $result = $this->request($this->settings->getUrl() . 'orders/list', $page);
        return $result;
    }

    /**
     * @param array $body
     * @return array|mixed
     */
    public function sendOrder(array $body)
    {
        $result = [];
        try {
            $response = $this->client->request('POST', $this->settings->getUrl() . 'orders', ['body' => Json::encode($body)]);
            $result = Json::decode($response->getBody()->getContents());
        } catch (GuzzleException $exception) {
            $result['error'] = $exception->getMessage();
        }

        return $result;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function orderPay($id)
    {

        $result = [];
        try {
            $response = $this->client->request('GET', $this->settings->getUrl() . 'orders/pay?id=' . $id, [
                'headers' => [
                    'Authorization' => sprintf('Bearer %s', $this->settings->getToken())
                ]
            ]);
            $result = Json::decode($response->getBody()->getContents());
        } catch (GuzzleException $exception) {
            $result['error'] = $exception->getMessage();
        }

        return $result;
    }
}