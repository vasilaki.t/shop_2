<?php

namespace app\api;
/**
 * Class CartApi
 * @package app\api
 */
class CartApi
{
    private static $cart = [];

    /**
     * @param $id
     * @param $quantity
     */
    public static function add2Cart($id, $quantity)
    {
        $session = \Yii::$app->session;
        if ($session->has(__CLASS__)) {
            $cart = $session->get(__CLASS__);
        } else {
            $cart = new \ArrayObject;
        }
        $cart[$id] = $quantity;
        $session->set(__CLASS__, $cart);
    }

    /**
     * @return mixed|null
     */
    public static function getCart()
    {
        $session = \Yii::$app->session;
        if (is_null(self::$cart) && $session->has(__CLASS__)) {
            self::$cart = $session->get(__CLASS__);
        }

        return self::$cart;
    }

    /**
     * @param $id
     * @return null
     */
    public static function getProduct($id)
    {
        $cart = self::getCart();
        return key_exists($id, $cart) ? $cart[$id] : null;
    }

    /**
     * Clear cart
     */
    public static function clearCart()
    {
        \Yii::$app->session->set(__CLASS__, new \ArrayObject);
    }

}