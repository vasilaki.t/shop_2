<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'shop' => [
        'auth' => [
            'login' => 1,
            'password' => 1,
            'token' => 11,
            'url' => 'http://undefined.vasilaki.tk/',
        ],
    ],
];
