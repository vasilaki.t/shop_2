<?php

namespace app\models;

use yii\base\Model;

/**
 * Class OrderForm
 * @package app\models
 */
class OrderForm extends Model
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $email;
    /**
     * @var array
     */
    public $goods;
    
    public function rules()
    {
        return [
            [['name', 'email', 'goods'], 'required'],
        ];
    }
}