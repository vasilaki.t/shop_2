<?php

namespace app\controllers;

use app\api\CartApi;
use app\api\ShopApi;
use app\dto\ApiDto;
use app\models\OrderForm;
use GuzzleHttp;

/**
 * Class OrderController
 */
class OrderController extends \yii\web\Controller
{
    /**
     * @param $quantity
     * @param $id
     */
    public function actionChangeCart($quantity, $id)
    {
        CartApi::add2Cart($id, $quantity);
    }


    public function actionOrder()
    {
        $model = new OrderForm();
        $request = \Yii::$app->request->post('OrderForm');
        if (!empty($request)) {
            $model->attributes = $request;
            $model->goods = (array)CartApi::getCart();
            $api = new ShopApi(new GuzzleHttp\Client(), new ApiDto(\Yii::$app->params['shop']['auth']));
            $result = $api->sendOrder((array)$model);


            if ($model->validate()) {

            }
        }
    }


    public function actionPay($id)
    {
        $api = new ShopApi(new GuzzleHttp\Client(), new ApiDto(\Yii::$app->params['shop']['auth']));
        $result = $api->orderPay($id);
    }
}