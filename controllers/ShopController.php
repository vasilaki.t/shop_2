<?php

namespace app\controllers;

use app\api\ShopApi;
use app\dto\ApiDto;
use app\models\LoginForm;
use app\models\OrderForm;
use GuzzleHttp;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Class ShopController
 * @package app\controllers
 */
class ShopController extends Controller
{

    /**
     * @param mixed $page
     * @return mixed
     */
    public function actionIndex($page = null)
    {

        if (\Yii::$app->user->isGuest == false || 1) {
            $api = new ShopApi(new GuzzleHttp\Client(), new ApiDto(\Yii::$app->params['shop']['auth']));
            $response = $api->getProductList($page);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $response['items'],
                'pagination' => false,
            ]);
            $orderForm = new OrderForm();
            $pages = new Pagination(['totalCount' => $response['_meta']['totalCount']]);

            $result = $this->render('index', compact('dataProvider', 'pages', 'orderForm'));
        } else {
            $model = new LoginForm();
            $request = \Yii::$app->request->post('LoginForm');
            if ($request) {
                $model->attributes = $request;
                $model->login();
            }
            $result = $this->render('login', compact('model'));
        }

        return $result;

    }

    public function actionList($page = null)
    {

        $api = new ShopApi(new GuzzleHttp\Client(), new ApiDto(\Yii::$app->params['shop']['auth']));
        $response = $api->getOrderList($page);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $response['items'],
            'pagination' => false,
        ]);
        return $this->render('list', compact('dataProvider', 'pages'));
    }
}